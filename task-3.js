var boxes = process.argv[2];
var blackBox = '';
var whiteBox = '';


var chessDesk = function (error) {
    if (error) {
        console.log(error);
    } else {
        for (var i = 0; i < boxes; i++) {
            var array = [];
            if (i % 2 === 1) {
                blackBox = '██';
                whiteBox = '  ';
            } else {
                blackBox = '  ';
                whiteBox = '██';
            }
            for (var j = 0; j < boxes; j++) {
                if (j % 2 === 0) {
                    array.push(whiteBox);
                } else {
                    array.push(blackBox);
                }
            }
            console.log(array.join(''));
        }

    }
};

chessDesk();